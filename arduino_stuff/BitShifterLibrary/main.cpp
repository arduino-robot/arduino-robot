#include <iostream>
#include <string>

using namespace std;

void BinaryOut(unsigned n)
{
	string sOut;
	
	if(!n)
		sOut = "0" + sOut;

	while(n)
	{
		if(n%2 == 0)
		{
			sOut = "0" + sOut;
		}
		else
		{
			sOut = "1" + sOut;
			--n;
		}

		n /= 2;
	}

	cout << sOut;
}

void HexOut(unsigned n)
{
	string sOut;
	
	if(!n)
		sOut = "0" + sOut;

	while(n)
	{
		int m = n % 16;

		if(m == 0)
			sOut = "0" + sOut;
		else if(m == 1)
			sOut = "1" + sOut;
		else if(m == 2)
			sOut = "2" + sOut;
		else if(m == 3)
			sOut = "3" + sOut;
		else if(m == 4)
			sOut = "4" + sOut;
		else if(m == 5)
			sOut = "5" + sOut;
		else if(m == 6)
			sOut = "6" + sOut;
		else if(m == 7)
			sOut = "7" + sOut;
		else if(m == 8)
			sOut = "8" + sOut;
		else if(m == 9)
			sOut = "9" + sOut;
		else if(m == 10)
			sOut = "A" + sOut;
		else if(m == 11)
			sOut = "B" + sOut;
		else if(m == 12)
			sOut = "C" + sOut;
		else if(m == 13)
			sOut = "D" + sOut;
		else if(m == 14)
			sOut = "E" + sOut;
		else if(m == 15)
			sOut = "F" + sOut;

		n -= m;

		n /= 16;
	}

	cout << sOut;
}

typedef unsigned char MyByte;

template <class T>
void SetBit(T& n, int nBitNum, bool bBit)
{
	if(bBit)
	{
		T i = 1;
		i <<= nBitNum;
		n |= i;
	}
	else
	{
		T i = 1;
		i <<= nBitNum;
		i = ~i;
		n &= i;
	}
}

template <class T>
bool GetBit(T n, int nBitNum)
{
	n >>= nBitNum;
	n &= 1;
	return n != 0;
}


void SetBitInfo(MyByte* vArr, int& nPos, int& nBitPos, int nNum, int nBitSize)
{
	for(int i = 0; i < nBitSize; ++i)
	{
		bool b = GetBit(nNum, i);
		SetBit(vArr[nPos], nBitPos, b);

		++nBitPos;
		if(nBitPos >= 8)
		{
			nBitPos = 0;
			++nPos;
		}
	}
}

void GetBitInfo(MyByte* vArr, int& nPos, int& nBitPos, int& nNum, int nBitSize)
{
	for(int i = 0; i < nBitSize; ++i)
	{
		bool b = GetBit(vArr[nPos], nBitPos);
		SetBit(nNum, i, b);

		++nBitPos;
		if(nBitPos >= 8)
		{
			nBitPos = 0;
			++nPos;
		}
	}
}

void main()
{
	/*
	for(int i = 0; i < 25; ++i)
	{
		int n = i;
		BinaryOut(n);
		cout << "\n";
		
		SetBit(n, 1, 0);
		BinaryOut(n);
		cout << "\n";

		cout << GetBit(n, 2) << "\n";
	}
	*/

	MyByte arr[100];

	int n;

	int nPos = 0;
	int nBitPos = 0;

	cin >> n;
	SetBitInfo(arr, nPos, nBitPos, n, 10);

	cin >> n;
	SetBitInfo(arr, nPos, nBitPos, n, 10);

	nPos = 0;
	nBitPos = 0;

	GetBitInfo(arr, nPos, nBitPos, n, 10);
	cout << n << "\n";

	GetBitInfo(arr, nPos, nBitPos, n, 10);
	cout << n << "\n";
}