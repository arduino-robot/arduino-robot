#include <AFMotor.h>

#include <Wire.h>
#include <LSM303DLH.h>

LSM303DLH compass;

AF_DCMotor motor1(3, MOTOR12_64KHZ); // create motor #3, 64KHz pwm
AF_DCMotor motor2(4, MOTOR12_64KHZ); // create motor #4, 64KHz pwm

struct MotionLocker
{
  bool bLocked;
  int nBound;
  bool bLessThan;
  
  bool Check(int& nVal)
  {
    if(bLessThan)
      return nVal < nBound;
    else
      return nVal > nBound;
  }
  
  MotionLocker():bLocked(false){}
};

MotionLocker mlDir1;
MotionLocker mlDir2;

int nDir1 = 0;
int nDir2 = 0;

int nOldDir1 = 0;
int nOldDir2 = 0;

char cCmd;

volatile int nMoveRight = 0;
volatile int nMoveLeft = 0;

int nMoveRightOld = 0;
int nMoveLeftOld = 0;

int nSonarPin = A3;

void MoveRight()
{
  if(nOldDir1 <= 0)
    ++nMoveRight;
  else
    --nMoveRight;
}

void MoveLeft()
{
  if(nOldDir2 <= 0)
    ++nMoveLeft;
  else
    --nMoveLeft;
}

void setup()
{
  Serial.begin(9600);           // set up Serial library at 9600 bps
  
  Wire.begin();
  compass.enable();
  
  // Calibration values. Use the serial example program to get the values for
  // your compass.
  compass.m_max.x = +160; compass.m_max.y = +300; compass.m_max.z = 240;
  compass.m_min.x = -400; compass.m_min.y = -250; compass.m_min.z = -220;

  motor1.setSpeed(200);     // set the speed to 200/255
  motor2.setSpeed(200);     // set the speed to 200/255
  
  attachInterrupt(1, MoveRight, CHANGE);  
  attachInterrupt(0, MoveLeft, CHANGE);  
}

void loop()
{
  int nMoveCopyRight = nMoveRight;
  int nMoveCopyLeft  = nMoveLeft;
  
  if(nMoveCopyRight != nMoveRightOld || nMoveCopyLeft != nMoveLeftOld)
  {
    nMoveRightOld = nMoveCopyRight;
    nMoveLeftOld  = nMoveCopyLeft;

    //compass.read();
    //int heading = compass.heading((vector){0,-1,0});
    
    Serial.print(nMoveCopyRight);
    Serial.print(" ");
    Serial.print(nMoveCopyLeft);
    Serial.print(" ");
    Serial.print(int(analogRead(nSonarPin)));
    /*
    Serial.print(" ");
    Serial.print(compass.m.x);
    Serial.print(" ");
    Serial.print(compass.m.y);
    Serial.print(" ");
    Serial.print(compass.m.z);
    */
    Serial.println("");

  }
  
  if(!mlDir1.bLocked)
    nDir1 = 0;
  else if(!mlDir1.Check(nMoveCopyRight))
  {
    nDir1 = 0;
    mlDir1.bLocked = false;
  }
  
  if(!mlDir2.bLocked)
    nDir2 = 0;
  else if(!mlDir2.Check(nMoveCopyLeft))
  {
    nDir2 = 0;
    mlDir2.bLocked = false;
  }
  
  if (Serial.available() > 0)
  {
      cCmd = Serial.read();
      
      mlDir1.bLocked = false;
      mlDir2.bLocked = false;
      
      if(cCmd == 'w')
      {
        nDir1 = -1;
        nDir2 = -1;
      }
      else if(cCmd == 's')
      {
        nDir1 = 1;
        nDir2 = 1;
      }
      else if(cCmd == 'a')
      {
        nDir1 = -1;
        nDir2 = 1;
      }
      else if(cCmd == 'd')
      {
        nDir1 = 1;
        nDir2 = -1;
      }
      else if(cCmd == 'r')
      {
        nDir1 = 1;
        nDir2 = -1;
        
        mlDir1.bLocked = true;
        mlDir1.nBound = nMoveCopyRight - 247;
        mlDir1.bLessThan = false;

        mlDir2.bLocked = true;
        mlDir2.nBound = nMoveCopyLeft + 299;
        mlDir2.bLessThan = true;
      }
      
      while(Serial.available() > 0)
        Serial.read();
  }
  
  if(nDir1 != 0)
    nOldDir1 = nDir1;
  if(nDir2 != 0)
    nOldDir2 = nDir2;
  
  if(nDir1 == 1)
    motor1.run(FORWARD);
  else if(nDir1 == -1)
    motor1.run(BACKWARD);
  else
    motor1.run(RELEASE);
  
  if(nDir2 == 1)
    motor2.run(FORWARD);
  else if(nDir2 == -1)
    motor2.run(BACKWARD);
  else
    motor2.run(RELEASE);
    
  delay(100);
  
}
