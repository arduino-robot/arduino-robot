int arrSymbolPins[] = {2,3,4,5,6,7,8,9};
int arrDigitPins[] = {10, 11, 12, 13};
int arrNumbers[80] = {
1,	1,	1,	0,	1,	1,	1,	0,
0,	0,	1,	0,	0,	1,	0,	0,
1,	1,	0,	1,	0,	1,	1,	0,
1,	0,	1,	1,	0,	1,	1,	0,
0,	0,	1,	1,	1,	1,	0,	0,
1,	0,	1,	1,	1,	0,	1,	0,
1,	1,	1,	1,	1,	0,	1,	0,
0,	0,	1,	0,	0,	1,	1,	0,
1,	1,	1,	1,	1,	1,	1,	0,
1,	0,	1,	1,	1,	1,	1,	0
};

int arrOutPutDigits[] = {0, 8, 9, 2};
int nCurrDigit = 0;

void WriteDigit(int n)
{
  for(int i = 0; i < 8; ++i)
    if(arrNumbers[n * 8 + i] == 1)
      digitalWrite(arrSymbolPins[i], HIGH);
    else
      digitalWrite(arrSymbolPins[i], LOW);
}

void NextDigit()
{
  digitalWrite(arrDigitPins[nCurrDigit], HIGH);
  nCurrDigit = (nCurrDigit + 1)%4;
  
  WriteDigit(arrOutPutDigits[nCurrDigit]);
  
  digitalWrite(arrDigitPins[nCurrDigit], LOW);
}

void ReadSensorData()
{
  int sensorValue = analogRead(A0);
  
  arrOutPutDigits[3] = sensorValue%10;
  arrOutPutDigits[2] = (sensorValue%100)/10;
  arrOutPutDigits[1] = (sensorValue%1000)/100;
  arrOutPutDigits[0] = (sensorValue%10000)/1000;  
}

void setup()
{
  //Serial.begin(9600);

  pinMode(2, OUTPUT);      
  pinMode(3, OUTPUT);      
  pinMode(4, OUTPUT);      
  pinMode(5, OUTPUT);      
  pinMode(6, OUTPUT);      
  pinMode(7, OUTPUT);      
  pinMode(8, OUTPUT);      
  pinMode(9, OUTPUT);      
  pinMode(10, OUTPUT);     
  pinMode(11, OUTPUT);     
  pinMode(12, OUTPUT);     
  pinMode(13, OUTPUT);     

  digitalWrite(10, HIGH);
  digitalWrite(11, HIGH);
  digitalWrite(12, HIGH);
  digitalWrite(13, HIGH);
}

int i = 0;

void loop() {
  
  ++i;
  if(i >= 200)
  {
    i = 0;
    ReadSensorData();
  }
  
  NextDigit();
  delay(1);
  
}
