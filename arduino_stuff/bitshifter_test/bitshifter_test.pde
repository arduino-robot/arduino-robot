#include <Robot.h>

void ProcessConditionalMovement()
{
  delay(10);
  
  if(Serial.available() < 2)
    return;

  byte btData[2];
  
  btData[0] = Serial.read();
  btData[1] = Serial.read();
  
  byte nPos = 0;
  byte nBitPos = 0;
  
  byte nDir = 0;
  byte nEncoder = 0;
  int nLimit = 0;
  byte nSignature = 0;
  
  GetBitInfo(btData, nPos, nBitPos, nDir, 2);
  GetBitInfo(btData, nPos, nBitPos, nEncoder, 1);
  GetBitInfo(btData, nPos, nBitPos, nLimit, 11);
  GetBitInfo(btData, nPos, nBitPos, nSignature, 2);
  
  Serial.print("t");
  
  Serial.println("Info:");
  Serial.println(int(nDir));
  Serial.println(int(nEncoder));
  Serial.println(int(nLimit));
  Serial.println(int(nSignature));
  Serial.println("");
}

void setup()
{
  Serial.begin(9600);           // set up Serial library at 9600 bps
}



void loop()
{
  if (Serial.available() > 0)
  {
      char cCmd = Serial.read();
      
      if(cCmd == 'c')
      {
        ProcessConditionalMovement();
      }
  }

}
