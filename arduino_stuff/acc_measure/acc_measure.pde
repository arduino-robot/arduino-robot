const long nT = 200;
const long nSec = 10;

const long nCounterMax = nT * nSec;
const int nDelayMs = 1000 / nT;

long nCounter = 0;

long nTimer;

int nMeasure = 0;

int AnalogPortMap[] = {A0, A1, A2};


bool critical(int nPort)
{
  int n = analogRead(AnalogPortMap[nPort]);
  
  if(n < 20 || n > 1000)
    return true;
  return false;
}

void StartMeasure()
{
  
  nMeasure = 1;
  nCounter = 0;
  
  digitalWrite(9, HIGH);
  digitalWrite(8, LOW);
  
  nTimer = millis();
  
}

void Measure()
{
  
  int nTime = millis();
  if( (nTime - nTimer) < nDelayMs)
    return;
  nTimer = nTime;
  
  //nTimes[nCounter] = nTimer;
  
  unsigned char c0 = analogRead(AnalogPortMap[0])/4;
  Serial.print(c0);

  unsigned char c2 = analogRead(AnalogPortMap[2])/4;
  Serial.print(c2);

  //nData[0][nCounter] = analogRead(A0);
  //nData[1][nCounter] = analogRead(A1);
  //nData[2][nCounter] = analogRead(A2);
  
  ++nCounter;
  
  if(nCounter >= nCounterMax)
  {
    nMeasure = 0;
    digitalWrite(9, LOW);
  }
  
}

void setup()
{
  pinMode(13, OUTPUT);     

  pinMode(8, OUTPUT);     
  pinMode(9, OUTPUT);     
  pinMode(10, OUTPUT);  

  pinMode(11, INPUT);  

  Serial.begin(9600);

}

void loop()
{
  digitalWrite(10, HIGH);

  for(int i = 0; i < 3; ++i)
  {
    if(critical(i))
    {
      digitalWrite(8, HIGH);
      break;
    }
  }
  
  
  if(nMeasure == 1)
    Measure();
  
  if(digitalRead(11) == HIGH)
  {
    if(nMeasure == 0)
      StartMeasure();
  }
  
  
  //unsigned char c = analogRead(AnalogPortMap[0])/4;
  
  //Serial.print(c);
  
}
