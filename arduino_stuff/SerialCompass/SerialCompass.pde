#include <Wire.h>
#include <LSM303DLH.h>

LSM303DLH compass;

void setup() {
  Serial.begin(9600);
  Wire.begin();
  compass.enable();
}

void loop() {
  compass.read();

  Serial.print(compass.m.x);
  Serial.print("\t");
  Serial.print(compass.m.y);
  Serial.print("\t");
  Serial.println(compass.m.z);
  
  delay(100);
}
