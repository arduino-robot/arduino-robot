/*
  AnalogReadSerial
 Reads an analog input on pin 0, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */
 
#include <math.h>
 
void measure(int nAxis)
{
  int nPort;
  int nZero;
  int nG;
  
  if(nAxis == 0)
  {
    nPort = A0;
    nZero = 502;
    nG = 502 - 395;
  }
  else if(nAxis == 1)
  {
    nPort = A1;
    nZero = 558;
    nG = 558 - 437;
  }
  else
  {
    nPort = A2;
    nZero = 545;
    nG = 545 - 434;
  }
  
  long nSum = 0;
  long nSumSq = 0;
  long n = 100;
  
  for(long i = 0; i < n; ++i)
  {
    long k = analogRead(nPort);
    nSum += k;
    nSumSq += k*k;
    delay(1);
  }
    
  nSum /= n;
  nSumSq /= n;
  
  float fVar = sqrt(float(nSumSq - nSum*nSum));
  
  if(nAxis == 0)
    Serial.print("Z ");
  else if(nAxis == 1)
    Serial.print("Y ");
  else
    Serial.print("X ");

  Serial.print(float(nSum - nZero)/nG);
  Serial.print(" +/- ");
  Serial.print(fVar/nG);
  Serial.println("g");
}

void setup() {
  Serial.begin(9600);
}

void loop()
{
  measure(0);
  measure(1);
  measure(2);
  Serial.println("");
  
  delay(1000);
}
