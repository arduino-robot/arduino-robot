/*
  AnalogReadSerial
 Reads an analog input on pin 0, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */
 
bool measure(int nPort)
{
  int n = analogRead(nPort);
  
  if(n < 20 || n > 1000)
    return true;
  return false;
}

void setup() {
  pinMode(13, OUTPUT);     

  pinMode(8, OUTPUT);     
  pinMode(9, OUTPUT);     
  pinMode(10, OUTPUT);  

  pinMode(11, INPUT);  

  Serial.begin(9600);
}

void loop()
{
  int nC = 0;
  for(int i = 0; i < 3; ++i)
  {
    if(measure(i))
    {
      ++nC;
      digitalWrite(8 + i, HIGH);
    }
    /*
    else
    {
      digitalWrite(8 + i, LOW);
    }
    */
    
  }
  
  
  if(nC != 0)
    digitalWrite(13, HIGH);
  else
    digitalWrite(13, LOW);
  
  if(digitalRead(11) == HIGH)
  {
    digitalWrite(8, LOW);
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
  }
}
