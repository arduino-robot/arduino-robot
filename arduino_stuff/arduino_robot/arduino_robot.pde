#include <AFMotor.h>

//#include <LSM303DLH.h>

#include <Robot.h>

const int nSonarPin = A3;

const int nMotor1Slot = 3;
const int nMotor2Slot = 4;

AF_DCMotor motor1(nMotor1Slot, MOTOR12_64KHZ); // create motor #3, 64KHz pwm
AF_DCMotor motor2(nMotor2Slot, MOTOR12_64KHZ); // create motor #4, 64KHz pwm

volatile int nRightEncoder = 0;
volatile int nLeftEncoder = 0;

int nRightEncoderOld = 0;
int nLeftEncoderOld = 0;

int nSonarOld = 0;

// Interrupt routines for encoders
void MoveRight(){++nRightEncoder;}
void MoveLeft(){++nLeftEncoder;}

int nDir1;
int nDir2;

struct MotionLock
{
  bool bLocked;
  volatile int* pEncoder;
  int nLimit;
  
  MotionLock():bLocked(false){}
  
  void ProcessMotion()
  {
    if(!bLocked)
      return;
    
    if(*pEncoder > nLimit)
      Cancel();
  }
  
  void Cancel()
  {
    if(bLocked)
    {
      bLocked = false;
      
      nDir1 = 0;
      nDir2 = 0;
    }
  }
};

MotionLock lck;

//LSM303DLH compass;

void setup()
{
  Serial.begin(9600);           // set up Serial library at 9600 bps
  
  //Wire.begin();
  //compass.enable();
  
  // Calibration values. Use the serial example program to get the values for
  // your compass.
  //compass.m_max.x = +160; compass.m_max.y = +300; compass.m_max.z = 240;
  //compass.m_min.x = -400; compass.m_min.y = -250; compass.m_min.z = -220;

  motor1.setSpeed(200);     // initial motor 1 speed (0-255)
  motor2.setSpeed(200);     // initial motor 2 speed (0-255)
  
  attachInterrupt(1, MoveRight, CHANGE);  
  attachInterrupt(0, MoveLeft, CHANGE);  
}

void SendInfo()
{
    int nRightEncoderCp = nRightEncoder;
    int nLeftEncoderCp  = nLeftEncoder;
    
    byte nRightDelta = nRightEncoderCp - nRightEncoderOld;
    byte nLeftDelta = nLeftEncoderCp - nLeftEncoderOld;
    
    nRightEncoderOld = nRightEncoderCp;
    nLeftEncoderOld = nLeftEncoderCp;
    
      
    byte btData[4] = {'d', nRightDelta, nLeftDelta, byte(nSonarOld)};
    
    Serial.write(btData, 4);

    //compass.read();
    //int heading = compass.heading((vector){0,-1,0});

    /*
    Serial.print(" ");
    Serial.print(compass.m.x);
    Serial.print(" ");
    Serial.print(compass.m.y);
    Serial.print(" ");
    Serial.print(compass.m.z);
    */
}

void MoveForward()
{
  motor1.setSpeed(200);
  motor2.setSpeed(155);

  nDir1 = 1;
  nDir2 = 1;
}

void MoveBackward()
{
  motor1.setSpeed(200);
  motor2.setSpeed(155);

  nDir1 = -1;
  nDir2 = -1;
}

void TurnRight()
{
  motor1.setSpeed(200);
  motor2.setSpeed(200);

  nDir1 = -1;
  nDir2 = 1;
}

void TurnLeft()
{
  motor1.setSpeed(200);
  motor2.setSpeed(200);

  nDir1 = 1;
  nDir2 = -1;
}


void ProcessConditionalMovement()
{
  delay(10);
  
  if(Serial.available() < 2)
    return;

  byte btData[2];
  
  btData[0] = Serial.read();
  btData[1] = Serial.read();
  
  byte nPos = 0;
  byte nBitPos = 0;
  
  byte nDir = 0;
  byte nEncoder = 0;
  int nLimit = 0;
  byte nSignature = 0;
  
  GetBitInfo(btData, nPos, nBitPos, nDir, 2);
  GetBitInfo(btData, nPos, nBitPos, nEncoder, 1);
  GetBitInfo(btData, nPos, nBitPos, nLimit, 11);
  GetBitInfo(btData, nPos, nBitPos, nSignature, 2);
  
  if(nSignature != 3)
    return;
    
  lck.bLocked = true;
  if(nEncoder)
    lck.pEncoder = &nRightEncoder;
  else
    lck.pEncoder = &nLeftEncoder;
  
  lck.nLimit = *lck.pEncoder + nLimit;
  
  if(nDir == 0)
    MoveForward();
  else if(nDir == 1)
    MoveBackward();
  else if(nDir == 2)
    TurnRight();
  else
    TurnLeft();
}



void loop()
{
  int nSonarData = analogRead(nSonarPin);
  if(nSonarData > 255)
    nSonarData = 255;
  
  if(nRightEncoder != nRightEncoderOld || nLeftEncoder != nLeftEncoderOld || nSonarData != nSonarOld)
  {
    nSonarOld = nSonarData;
    
    SendInfo();
    lck.ProcessMotion();
  }
  
  if(!lck.bLocked)
  {
    nDir1 = 0;
    nDir2 = 0;
  }
  
  if (Serial.available() > 0)
  {
      lck.Cancel();
    
      char cCmd = Serial.read();
      
      if(cCmd == 'w')
        MoveForward();
      else if(cCmd == 's')
        MoveBackward();
      else if(cCmd == 'a')
        TurnLeft();
      else if(cCmd == 'd')
        TurnRight();
      else if(cCmd == 'i')
        SendInfo();
      /*
      else if(cCmd == 'm')
      {
        delay(10);
        if(Serial.available() >= 1)
        {
          byte btSpeed = Serial.read();
          
          motor1.setSpeed(btSpeed);
          motor2.setSpeed(btSpeed);
        }
      }
      else if(cCmd == '<')
      {
        delay(10);
        if(Serial.available() >= 1)
        {
          byte btSpeed = Serial.read();
          
          motor1.setSpeed(btSpeed);
        }
      }
      else if(cCmd == '>')
      {
        delay(10);
        if(Serial.available() >= 1)
        {
          byte btSpeed = Serial.read();
          
          motor2.setSpeed(btSpeed);
        }
      }
      */
      else if(cCmd == 'c')
      {
        ProcessConditionalMovement();
      }

      while(Serial.available() > 0)
        Serial.read();
  }
  
  if(nDir1 == 1)
    motor1.run(FORWARD);
  else if(nDir1 == -1)
    motor1.run(BACKWARD);
  else
    motor1.run(RELEASE);
  
  if(nDir2 == 1)
    motor2.run(FORWARD);
  else if(nDir2 == -1)
    motor2.run(BACKWARD);
  else
    motor2.run(RELEASE);
    
  delay(100);
}
