#include <math.h>
 
void measure(int nAxis)
{
  int nPort;
  
  if(nAxis == 0)
  {
    nPort = A0;
  }
  else if(nAxis == 1)
  {
    nPort = A1;
  }
  else
  {
    nPort = A2;
  }
  
  long k = analogRead(nPort);
    
  if(nAxis == 2)
    Serial.print("Z\t");
  else if(nAxis == 1)
    Serial.print("Y\t");
  else
    Serial.print("X\t");

  Serial.print(k);
  Serial.print("\t");
}

void setup() {
  Serial.begin(19200);
}

void loop()
{
  measure(0);
  measure(1);
  measure(2);
  Serial.print("T\t");
  Serial.println(millis());
  
  delay(5);
}
