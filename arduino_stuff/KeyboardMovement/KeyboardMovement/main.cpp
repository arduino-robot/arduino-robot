#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <time.h>

#include "SDL.h"

#include "Serial.h"

using namespace std;

void Remove(list<char>& lsSend, char c)
{
	for(list<char>::iterator itr = lsSend.begin(), etr = lsSend.end(); itr != etr;)
	{
		if(*itr == c)
			lsSend.erase(itr++);
		else
			++itr;
	}
}


#define TRUE 1
#define FALSE 0

int main(int argc, char *argv[])
{
    srand( (unsigned)time( NULL ));

	SDL_Event event;

    bool bTrue = true;
    bool bExit = false;

	list<char> lsSend;
    
    try
    {
        if( SDL_Init(SDL_INIT_VIDEO) < 0 )
		{
			std::cout << "cannot initialize video\n";
			return -1;
		}

		Serial srl("COM5");

		if(!srl.IsConnected())
		{
			std::cout << "Cannot connect!\n";
			return -1;
		}

        SDL_Surface* pScrImg = SDL_SetVideoMode(200, 200, 32, SDL_DOUBLEBUF | SDL_HWSURFACE);

        Uint32 nTimer = SDL_GetTicks();

        while(!bExit)
        {
            if(SDL_GetTicks() - nTimer > 90)
            {
                nTimer = SDL_GetTicks();

				if(lsSend.size())
				{
					string s;
					s += char(lsSend.front());

					if(!srl.WriteData(s.c_str(), s.size()))
					{
						std::cout << "Cannot write!\n";
						return -1;
					}

					std::cout << s << "\n";

					SDL_Delay(50);
				}
				
				while(true)
				{
					char nBuff[2];
					int nRead = srl.ReadData(nBuff, 1);

					if(!srl.IsConnected())
					{
						cout << "Disconnect on read!\n";
						return -1;
					}

					if(nRead > 0)
						std::cout << nBuff[0];
					else
						break;
				}
			}
            
            if(SDL_PollEvent(&event))
            {
                if(event.type == SDL_QUIT)
                    break;
                else if(event.type == SDL_KEYUP)
				{
					Remove(lsSend, event.key.keysym.sym);
				}
                else if(event.type == SDL_KEYDOWN)
                {
					Remove(lsSend, event.key.keysym.sym);
					lsSend.push_front(event.key.keysym.sym);
                }
                else if(event.type == SDL_MOUSEMOTION)
                {
                }
				else if(event.type == SDL_MOUSEBUTTONDOWN)
				{
				}
		    }

		}
    }
    catch(...)
    {
        std::cout << "Unknown error!\n";
    }

    return 0;
}
