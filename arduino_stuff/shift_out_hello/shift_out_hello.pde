//Pin connected to ST_CP of 74HC595
int latchPin = 8;
//Pin connected to SH_CP of 74HC595
int clockPin = 12;
////Pin connected to DS of 74HC595
int dataPin = 11;

int numbers_codes[10] = {235, 40, 179, 186, 120, 218, 219, 168, 251, 250};

void SerialSend(long nTime)
{
  for(long i = 0; i < nTime; ++i)
    Serial.print(char(i%256));
}

void setup() {
  //set pins to output so you can control the shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  pinMode(7, INPUT);

  Serial.begin(9600);	// opens serial port, sets data rate to 9600 bps
}

char incomingByte;

void loop()
{
    if (Serial.available() > 0)
    {
      incomingByte = Serial.read();

      digitalWrite(latchPin, LOW);

      if(incomingByte < '0' || incomingByte > '9')
        shiftOut(dataPin, clockPin, MSBFIRST, 255);
      else
        shiftOut(dataPin, clockPin, MSBFIRST, numbers_codes[int(incomingByte - '0')]);
 
      digitalWrite(latchPin, HIGH);

      delay(300);
    }
    
    if(digitalRead(7) == HIGH)
    {
      SerialSend(1000);
    }
  /*
  // count from 0 to 255 and display the number 
  // on the LEDs
  //for (int numberToDisplay = 0; numberToDisplay < 256; numberToDisplay++) {
  for (int numberToDisplay = 0; numberToDisplay < 10; numberToDisplay++) {
    // take the latchPin low so 
    // the LEDs don't change while you're sending in bits:
    digitalWrite(latchPin, LOW);
    // shift out the bits:
    //shiftOut(dataPin, clockPin, MSBFIRST, numberToDisplay);  
    shiftOut(dataPin, clockPin, MSBFIRST, numbers_codes[numberToDisplay]);  

    //take the latch pin high so the LEDs will light up:
    digitalWrite(latchPin, HIGH);
    // pause before next value:
    delay(1000);
  }
  */
}
