#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Serial.h"
#include <time.h>
#include <math.h>

using namespace std;

string sComName = "COM6";

void main()
{
	cout << "start\n";
	
	Serial srl(sComName.c_str(), true);

	if(!srl.IsConnected())
	{
		cout << "Cannot connect!\n";
		return;
	}

	cout << "Connected\n";

	ofstream ofs("out.txt");
	
	char nBuff[2];

	while(true)
	{
		int nRead = srl.ReadData(nBuff, 1);

		if(!srl.IsConnected())
		{
			cout << "Disconnect on read!\n";
			return;
		}

		if(nRead > 0)
		{
			ofs << nBuff[0];
		}
	}

	/*
	int nT = 200;
	int nSec = 10;

	double dt = int(1000 / nT);

	const int nDataSize = nT * nSec;

	size_t nDataPnt = 0;
	vector<MotionData> vData;
	vData.push_back(MotionData(127.318, 29.849, dt/1000, nDataSize));
	vData.push_back(MotionData(136.219, 28.399, dt/1000, nDataSize));

	char nBuff[2];

	for(int i = 0; i < nDataSize;)
	{

		int nRead = srl.ReadData(nBuff, 1);

		if(!srl.IsConnected())
		{
			cout << "Disconnect!\n";
			return;
		}

		if(nRead > 0)
		{
			vData[nDataPnt].Add(int(unsigned char(nBuff[0])));
			++nDataPnt;
			if(nDataPnt >= vData.size())
			{
				nDataPnt = 0;
				++i;

				if(i % 100 == 0)
					cout << i << "\n";

				if(i == nT*5)
				{
					for(size_t k = 0; k < vData.size(); ++k)
					{
						vData[k].dZero = vData[k].AverageAcceleration();
						vData[k].ResetData();
					}
					cout << "Reset\n";
				}
			}
			

		}
	}

	cout << "done\n\n";

	ofstream ofs("data.txt");

	for(size_t i = 0; i < vData.size(); ++i)
	{
		cout << i << ": " << vData[i].AverageAcceleration() << " " << vData[i].Std() << "\n";
		
		ofs << "\n" << i << ":\n\n";

		for(size_t j = 0; j < vData[i].vAccData.size(); j += 10)
			ofs << double(j) * dt/1000 << "\t" << vData[i].vRawData[j]  << "\t" << vData[i].vAccData[j] << "\t" << vData[i].vVelData[j] << "\t" << vData[i].vPosData[j] << "\t" << vData[i].vPosData[j]/2.54 << "\n";
	}
	*/
}