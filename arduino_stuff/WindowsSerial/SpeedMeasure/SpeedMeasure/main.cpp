#include <iostream>
#include <fstream>
#include <vector>
#include "Serial.h"
#include <time.h>

using namespace std;

double diffclock(clock_t clock1, clock_t clock2)
{
	double diffticks=clock1-clock2;
	double diffms=(diffticks)/CLOCKS_PER_SEC;
	return diffms;
}

void main()
{
	Serial srl("COM5");

	if(!srl.IsConnected())
	{
		cout << "Cannot connect!\n";
		return;
	}
	else
	{
		cout << "Connected!\n";
	}

	char nBuff[2];

	int nDataSize = 1000;

	while(true)
	{
		clock_t begin;

		int n = 0;
		int i;
		for(i = 0; i < nDataSize;)
		{

			int nRead = srl.ReadData(nBuff, 1);

			if(!srl.IsConnected())
			{
				cout << "Disconnect!\n";
				return;
			}

			if(nRead > 0)
			{
				if(i == 0)
				{
					cout << "start\n";
					begin=clock();
				}

				int n = int(unsigned char(nBuff[0]));

				
				if(n != i%256)
				{
					cout << "Miscommunication!\n";
					cout << "Got " << n << " while expecting " << i%256 << "\n";
					//break;
				}
				

				++i;
			}
		}

		clock_t end=clock();

		cout << "Time elapsed: " << double(diffclock(end,begin)) << " s"<< endl;
		cout << "Bytes read: " << i << endl;
		cout << "Speed: " << i/double(diffclock(end,begin)) << " B/s" << endl;

		cin.get();
	}

}