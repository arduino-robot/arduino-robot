#include <vector>


int RandomNum(int nRange);

float GetAngle(float x, float y);

float GetAngle(float x1, float y1, float x2, float y2);

struct Movement
{
	float fNum;
	bool bTurn;

	Movement(float fNum_, bool bTurn_):fNum(fNum_), bTurn(bTurn_){}
};

struct PosPoint
{
	float x;
	float y;

	PosPoint(){}
	PosPoint(float x_, float y_):x(x_), y(y_){}
};

void ConvertToPoints(std::vector<Movement>& vMoves, std::vector<PosPoint>& vPoints);
void ConvertToMoves(std::vector<PosPoint>& vPoints, std::vector<Movement>& vMoves);

class Variable
{
public:
	virtual void Mutate()=0;
	virtual float Deviation()=0;
};

struct Quantity
{
	float fMean;
	float fStDev;

	float fCurr;

	float fStep;

	/*virtual*/ void Mutate();
	/*virtual*/ float Deviation();

	Quantity(float fMean_, float fStDev_, float fStep_)
		:fMean(fMean_), fStDev(fStDev_), fCurr(fMean_), fStep(fStep_)
	{}
};

struct BoolMatrix
{
	std::vector<bool> vGrid;
	int nSzX;
	int nSzY;
};

struct SonarMeasurement
{
	Quantity qRadius;
	bool bAllowSides;

	float fBeamAngle;
	float fSideBeamAngle;
	
	/*virtual*/ void Mutate();
	/*virtual*/ float Deviation();

	void ApplyToGrid();
	bool bNoConflict;
};