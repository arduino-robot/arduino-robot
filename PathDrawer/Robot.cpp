#include "Robot.h"

#include <math.h>
#include <stdlib.h>

int RandomNum(int nRange)
{
	float f = rand();
	f /= (float(RAND_MAX) + 1);
	f *= nRange;

	return int(f);
}

float GetAngle(float x, float y)
{
	return atan2f(y, x);
}

float GetAngle(float x1, float y1, float x2, float y2)
{
	float fDot = x1*x2 + y1*y2;
	float fL1 = sqrt(x1*x1 + y1*y1);
	float fL2 = sqrt(x2*x2 + y2*y2);

	return acos(fDot/fL1/fL2);
}


void ConvertToPoints(std::vector<Movement>& vMoves, std::vector<PosPoint>& vPoints)
{
	float fCurrDir = 0;
	vPoints.push_back(PosPoint(0,0));

	for(unsigned i = 0; i < vMoves.size(); ++i)
	{
		Movement mv = vMoves[i];
		if(mv.bTurn)
			fCurrDir += mv.fNum;
		else
		{
			unsigned n = vPoints.size() - 1;
			float x = vPoints[i].x + mv.fNum * cos(fCurrDir); 
			float y = vPoints[i].y + mv.fNum * sin(fCurrDir);
			vPoints.push_back(PosPoint(x, y));
		}
	}
}

void ConvertToMoves(std::vector<PosPoint>& vPoints, std::vector<Movement>& vMoves)
{
	for(unsigned i = 0; i < vPoints.size(); ++i)
	{
		if(i + 1 >= vPoints.size())
			break;

		vMoves.push_back(Movement(GetAngle(vPoints[i+1].x - vPoints[i].x, vPoints[i+1].y - vPoints[i].y), true));
		vMoves.push_back(Movement(sqrt(pow(vPoints[i+1].x - vPoints[i].x, 2) + pow(vPoints[i+1].y - vPoints[i].y, 2)), false));
	}
}


/*virtual*/ void Quantity::Mutate()
{
	if(RandomNum(2) == 0)
		fCurr += fStep;
	else
		fCurr -= fStep;
}

/*virtual*/ float Quantity::Deviation()
{
	return pow((fCurr - fMean)/fStDev, 2);
}
