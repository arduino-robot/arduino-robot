#include "Model.h"

const float TURN_ERROR = .015;
const float MOVEMENT_ERROR = .010;
//const float TURN_ERROR = .1;
//const float MOVEMENT_ERROR = .1;

const float FORWARD_SONAR_DST = 11;


template<class T>
struct MinMeasurement
{
	bool bInit;
	T tVal;

	MinMeasurement():bInit(false){}

	void New(T tNew)
	{
		if(!bInit)
		{
			bInit = true;
			tVal = tNew;
		}
		else if(tNew < tVal)
		{
			tVal = tNew;
		}
	}

	T GetVal()
	{
		if(!bInit)
			throw std::string("not initialized minimum");
		return tVal;
	}

};

float GetAngle(ofPoint p)
{
	return atan2(p.x, -p.y) / 2 / PI * 360;
}

ofPoint GetPoint(float fDistance, float fAngle)
{
	ofPoint p;
	p.x = fDistance * sin(fAngle / 360 * 2 * PI);
	p.y = -fDistance * cos(fAngle / 360 * 2 * PI);
	return p;
}


float AngleDiff(float fAngleAt, float fAngleNew)
{
	float f = fAngleNew - fAngleAt;
	
	while(f <= -180)
		f += 360;
	while(f > 180)
		f -= 360;

	return f;
}

void WriteSonarData(std::ostream& ofs, const std::vector<int>& vData)
{
	ofs << vData.size() << " ";
	for(unsigned i = 0; i < vData.size(); ++i)
		ofs << vData[i] << " ";
	ofs << "\n";
}

void ReadSonarData(std::istream& ifs, std::vector<int>& vData)
{
	unsigned sz;
	ifs >> sz;
	vData.clear();
	for(unsigned i = 0; i < sz; ++i)
	{
		int n;
		ifs >> n;
		vData.push_back(n);
	}
}

void SonarScan::FillCones(std::vector<SonarCone>& v, ofPoint p, float fDir)
{
	if(nNum == 0)
		return;
	
	fDir += fInitialAngle;
	float fStep = GetAngle();

	for(int i = 0; i < nNum; ++i)
	{
		v.push_back(SonarCone(p + GetPoint(FORWARD_SONAR_DST, fDir), fDir, vResults[i]));
		fDir += fStep;
	}
}

void MovementScan::FillCones(std::vector<SonarCone>& v, ofPoint p, float fDir, float fDst)
{
	if(nNum == 0)
		return;
	
	float fStep = fDst / nNum;

	for(int i = 0; i < nNum; ++i)
		v.push_back(SonarCone(p + GetPoint(FORWARD_SONAR_DST + fStep*i, fDir), fDir, vResults[i]));
}



std::ostream& operator << (std::ostream& ofs, const SonarScan& ss)
{
	ofs << ss.fInitialAngle << " ";
	ofs << ss.fScanAngle << " ";
	WriteSonarData(ofs, ss.vResults);
	return ofs;
}

std::istream& operator >> (std::istream& ifs, SonarScan& ss)
{
	ifs >> ss.fInitialAngle;
	ifs >> ss.fScanAngle;
	ReadSonarData(ifs, ss.vResults);
	return ifs;
}

std::ostream& operator << (std::ostream& ofs, const MovementScan& ms)
{
	WriteSonarData(ofs, ms.vResults);
	return ofs;
}

std::istream& operator >> (std::istream& ifs, MovementScan& ms)
{
	ReadSonarData(ifs, ms.vResults);
	return ifs;
}


std::ostream& operator << (std::ostream& ofs, const Node& nd)
{
	ofs << nd.fTurnAngle << " ";
	ofs << nd.fDistance << " ";
	ofs << nd.ss;
	ofs << nd.ms;
	return ofs;
}

std::istream& operator >> (std::istream& ifs, Node& nd)
{
	ifs >> nd.fTurnAngle;
	ifs >> nd.fDistance;
	ifs >> nd.ss;
	ifs >> nd.ms;
	return ifs;
}

std::ostream& operator << (std::ostream& ofs, const Trajectory& trj)
{
	ofs << trj.vNodes.size() << " ";
	for(unsigned i = 0; i < trj.vNodes.size(); ++i)
		ofs << trj.vNodes[i] << " ";
	ofs << "\n";
	return ofs;
}

std::istream& operator >> (std::istream& ifs, Trajectory& trj)
{
	unsigned sz;
	ifs >> sz;
	trj.vNodes.clear();
	for(unsigned i = 0; i < sz; ++i)
	{
		Node n;
		ifs >> n;
		trj.vNodes.push_back(n);
	}

	trj.CalculatePoints();

	return ifs;
}


float Node::GetTotalAngle()
{
	return fTurnAngle + ss.fInitialAngle + ss.fScanAngle;
}

void Node::SetTotalAngle(float fNewAngle)
{
	float fRatio = fNewAngle/GetTotalAngle();

	fTurnAngle *= fRatio;
	ss.fInitialAngle *= fRatio;
	ss.fScanAngle *= fRatio;
}

void Node::FillCones(std::vector<SonarCone>& v)
{
	ss.FillCones(v, p, fDirection);
	ms.FillCones(v, p, fDirection, fDistance);
}

int Node::GetSonarDoF()
{
	return 2*(ss.nNum + ms.nNum);
}

int Node::GetDoF()
{
	int nSum = 0;

	if(fDistance != 0)
		++nSum;

	if(GetTotalAngle() != 0)
		++nSum;

	return nSum + GetSonarDoF();
}




Trajectory::Trajectory()
{
	vNodes.push_back(Node(ofPoint(0,0)));
}

void Trajectory::AddNode(ofPoint p)
{
	vNodes.push_back(Node(p));
	CalculateMoves(vNodes.size() - 1);
};



void Trajectory::CalculateMoves(int i)
{
	if(i <= 0 || i >= vNodes.size())
		return;

	vNodes[i - 1].fDistance = ofDist(vNodes[i].p.x, vNodes[i].p.y, vNodes[i-1].p.x, vNodes[i-1].p.y);

	vNodes[i].fDirection = GetAngle(vNodes[i].p - vNodes[i-1].p);
	
	vNodes[i-1].fTurnAngle = AngleDiff(vNodes[i-1].fDirection + vNodes[i-1].ss.GetTotalAngle(), vNodes[i].fDirection);
}

void Trajectory::CalculateMoves()
{
	if(vNodes.size() <= 1)
		return;

	vNodes[0].fDirection = 0;

	for(int i = 1; i < vNodes.size(); ++i)
		CalculateMoves(i);

	vNodes.back().fTurnAngle = 0;
	vNodes.back().fDistance = 0;
}

void Trajectory::FillCones(std::vector<SonarCone>& v)
{
	for(int i = 1; i < vNodes.size(); ++i)
		vNodes[i].FillCones(v);
}

int Trajectory::GetSonarDoF()
{
	int nSum = 0;

	for(int i = 1; i < vNodes.size(); ++i)
		nSum += vNodes[i].GetSonarDoF();

	return nSum;
}

int Trajectory::GetDoF()
{
	int nSum = 0;

	for(int i = 1; i < vNodes.size(); ++i)
		nSum += vNodes[i].GetDoF();

	return nSum;
}



void Trajectory::CalculatePoints()
{
	ofPoint pCurr(0,0);
	float fAngle = 0;

	for(int i = 0; i < vNodes.size(); ++i)
	{
		vNodes[i].p = pCurr;
		vNodes[i].fDirection = fAngle;

		fAngle += vNodes[i].GetTotalAngle();
		pCurr += GetPoint(vNodes[i].fDistance, fAngle);
	}
}



float GetVariance(Node& n1, Node& n2)
{
	float fV1, fV2;

	float fAngle1 = n1.GetTotalAngle();
	float fAngle2 = n2.GetTotalAngle();
	
	if(fAngle1 != 0)
		fV1 = pow(abs(fAngle1 - fAngle2)/(fAngle1 * TURN_ERROR), 2);
	else
		return 1000000;

	if(n1.fDistance != 0)
		fV2 = pow(abs(n1.fDistance - n2.fDistance)/(n1.fDistance * MOVEMENT_ERROR), 2);
	else 
		return 1000000;

	return fV1 + fV2;
}

float GetVariance(Trajectory& trj1, Trajectory& trj2)
{
	if(trj1.vNodes.size() != trj2.vNodes.size())
		throw int(1);
	
	float fSum = 0;

	for(size_t i = 0, sz = trj1.vNodes.size(); i < sz; ++i)
		fSum += GetVariance(trj1.vNodes[i], trj2.vNodes[i]);

	return fSum;
}

float GetSonarVariance(const Grid& gd, Trajectory& trj)
{
	Grid gdCopy = gd;
	gdCopy.ClearGrid();

	std::vector<SonarCone> v;

	trj.FillCones(v);

	size_t i = 0, sz = v.size();
	
	for(i = 0; i < sz; ++i)
		gdCopy.ApplySonar(v[i]);

	float fSum = 0;

	for(i = 0; i < sz; ++i)
		fSum += gdCopy.SonarVariation(v[i]);

	return fSum;
}

int SonarMeasurement::Extract()
{
	std::sort(vResults.begin(), vResults.end());
	return vResults[(vResults.size() - 1)/2];
}


void Trajectory::Wiggle(float fMv, bool bUpdate)
{
	int i = 1 + ofRandom(0,1)*(vNodes.size() - 1);

	vNodes[i].p.x += ofRandomf()*fMv;
	vNodes[i].p.y += ofRandomf()*fMv;

	if(bUpdate)
	{
		CalculateMoves(i);
		CalculateMoves(i + 1);
		CalculateMoves(i + 2);
	}
}

void Trajectory::WiggleAngle(float fFr)
{
	int i = ofRandom(0,1)*(vNodes.size() - 1);

	float fAngle = vNodes[i].GetTotalAngle();

	float fDelta = ofRandomf()*fAngle*TURN_ERROR*fFr;

	vNodes[i].SetTotalAngle(fAngle + fDelta);

	for(int j = i + 1; j < vNodes.size(); ++j)
	{
		float dDst = ofDist(vNodes[i].p.x, vNodes[i].p.y, vNodes[j].p.x, vNodes[j].p.y);
		float fAng = GetAngle(vNodes[j].p - vNodes[i].p);
		vNodes[j].p = vNodes[i].p + GetPoint(dDst, fAng + fDelta);
		vNodes[j].fDirection += fDelta;
	}
}

void Trajectory::WiggleDistance(float fFr)
{
	int i = ofRandom(0,1)*(vNodes.size() - 1);

	float fDst = vNodes[i].fDistance;
	float fDelta = ofRandomf()*fDst*MOVEMENT_ERROR*fFr;

	ofPoint pShift = vNodes[i + 1].p - vNodes[i].p;
	pShift *= fDelta/fDst;

	vNodes[i].fDistance += fDelta;

	for(int j = i + 1; j < vNodes.size(); ++j)
		vNodes[j].p += pShift;
}

ofPoint Grid::GetCoordinate(int n)
{
	int x = n % nWidth;
	int y = n / nWidth;

	ofPoint pRet = pCorner;
	pRet.x += pSize.x * x;
	pRet.y += pSize.y * y;

	return pRet;
}

ofPoint Grid::GetCenter(int n)
{
	ofPoint pRet = GetCoordinate(n);
	pRet.x += pSize.x/2;
	pRet.y += pSize.y/2;

	return pRet;
}

float Grid::GetDeviation(unsigned i, SonarCone sc)
{
	ofPoint p = GetCenter(i);
	int fAng = AngleDiff(sc.fAngle, GetAngle(p - sc.pCenter));
	if(fAng < -20 || fAng > 20)
		return -1;
	
	int nMeasAdj = sc.nMeas;
	
	if(fAng < -15 || fAng > 15)
		if(nMeasAdj > 12)
			nMeasAdj -= 2;

	int nDst = GetSonarDistance(ofDist(sc.pCenter.x, sc.pCenter.y, p.x, p.y));

	if(nDst > nMeasAdj)
		return -1;

	float f = float(nMeasAdj - nDst)/2;
	f /= 2;

	return f;
}


void Grid::ApplySonar(SonarCone sc)
{
	for(unsigned i = 0, sz = vData.size(); i < sz; ++i)
	{
		float f = GetDeviation(i, sc);
		
		if(f < 0)
			continue;

		if(f > vData[i])
			vData[i] = f;
	}
}

float Grid::SonarVariation(SonarCone sc)
{	
	MinMeasurement<float> mm;
	
	for(unsigned i = 0, sz = vData.size(); i < sz; ++i)
	{
		float f = GetDeviation(i, sc);
		
		if(f < 0)
			continue;

		mm.New(pow(f, 2) + pow(vData[i], 2));
	}

	if(!mm.bInit)
		return 0;
	else
		return mm.GetVal();
	
}


int ProcessSonar(int nMeas)
{
	if(nMeas == 15)
		nMeas = 14;
	else if(nMeas <= 13)
		nMeas = 12;
	
	return nMeas;
}

int GetSonarDistance(float fDist)
{
	fDist /= 2.54;

	if(fDist < 11.5)
	{
		if(fDist > 10.5)
			return 20;
		else if(fDist > 9.5)
			return 18;
		else if(fDist > 8)
			return 16;
		else if(fDist > 7)
			return 14;
		else
			return 12;
	}

	return int(fDist - .5) * 2;
}