#include "ofMain.h"
#include <fstream>
#include <vector>
#include <algorithm>

extern const float TURN_ERROR;
extern const float MOVEMENT_ERROR;

extern const float FORWARD_SONAR_DST;

float GetAngle(ofPoint p);
ofPoint GetPoint(float fDistance, float fAngle);

float AngleDiff(float fAngleAt, float fAngleNew);

void WriteSonarData(std::ostream& ofs, const std::vector<int>& vData);
void ReadSonarData(std::istream& ifs, std::vector<int>& vData);

struct SonarCone;
struct Grid;

struct SonarScan
{
	SonarScan():fScanAngle(0), fInitialAngle(0), nNum(0){}
	
	float fInitialAngle;
	float fScanAngle;
	int nNum;

	float GetAngle(){return fScanAngle/nNum;}
	float GetTotalAngle(){return fScanAngle + fInitialAngle;}

	std::vector<int> vResults;

	void FillCones(std::vector<SonarCone>& v, ofPoint p, float fDir);
};

std::ostream& operator << (std::ostream& ofs, const SonarScan& ss);
std::istream& operator >> (std::istream& ifs, SonarScan& ss);

struct MovementScan
{
	MovementScan():nNum(0){}

	int nNum;
	std::vector<int> vResults;

	void FillCones(std::vector<SonarCone>& v, ofPoint p, float fDir, float fDst);
};

std::ostream& operator << (std::ostream& ofs, const MovementScan& ms);
std::istream& operator >> (std::istream& ifs, MovementScan& ms);

struct Node
{
	ofPoint p;

	float fDirection;

	float fTurnAngle;
	float fDistance;

	SonarScan ss;
	MovementScan ms;

	Node(){}
	Node(ofPoint p_):p(p_), fDirection(0), fTurnAngle(0), fDistance(0){}

	float GetTotalAngle();
	void SetTotalAngle(float fNewAngle);

	void FillCones(std::vector<SonarCone>& v);
	
	int GetSonarDoF();
	int GetDoF();
};

std::ostream& operator << (std::ostream& ofs, const Node& nd);
std::istream& operator >> (std::istream& ifs, Node& nd);

struct Trajectory
{
	std::vector<Node> vNodes;

	Trajectory();

	void AddNode(ofPoint p);

	void CalculateMoves(int i);
	void CalculateMoves();
	void CalculatePoints();

	void Wiggle(float fMv, bool bUpdate = false);
	void WiggleAngle(float fFr);
	void WiggleDistance(float fFr);

	void FillCones(std::vector<SonarCone>& v);

	int GetSonarDoF();
	int GetDoF();
};

std::ostream& operator << (std::ostream& ofs, const Trajectory& trj);
std::istream& operator >> (std::istream& ifs, Trajectory& trj);

float GetVariance(Node& n1, Node& n2);
float GetVariance(Trajectory& trj1, Trajectory& trj2);

float GetSonarVariance(const Grid& gd, Trajectory& trj);

struct SonarMeasurement
{
	std::vector<int> vResults;

	void Add(int n){vResults.push_back(n);}

	int Extract();
};

struct SonarCone
{
	ofPoint pCenter;
	float fAngle;
	int nMeas;

	SonarCone(ofPoint pCenter_, float fAngle_, int nMeas_)
		:pCenter(pCenter_), fAngle(fAngle_), nMeas(nMeas_){}
};

struct Grid
{
	int nWidth;
	std::vector<float> vData;

	ofPoint pCorner;
	ofPoint pSize;

	Grid(int nWidth_, int nHeight, ofPoint pSize_, ofPoint pCenter):
	nWidth(nWidth_), pSize(pSize_), vData(nWidth_*nHeight, 0), pCorner(pCenter.x - nWidth_*pSize_.x/2, pCenter.y - nHeight*pSize_.y/2)
	{}

	ofPoint GetCoordinate(int n);
	ofPoint GetCenter(int n);

	float GetDeviation(unsigned i, SonarCone sc);

	void ApplySonar(SonarCone sc);
	float SonarVariation(SonarCone sc);

	void ClearGrid(){vData = std::vector<float>(vData.size(), 0);}
};

void ApplySonar(Grid& gd, SonarScan& ss);
float SonarVariation(Grid& gd, SonarScan& ss);

void ApplySonar(Grid& gd, MovementScan& ms);
float SonarVariation(Grid& gd, MovementScan& ms);

int ProcessSonar(int nMeas);

int GetSonarDistance(float fDist);