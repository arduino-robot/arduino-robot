#include "testApp.h"

#include <math.h>

typedef char uint8_t;

const float RIGHT_TURN_CNST = float(936) / 360;
const float LEFT_TURN_CNST = float(845) / 360;
const float MOVEMENT_CNST = 9.15;


enum DIR_TYPES {DTM_FORWARD = 0, DTM_BACK = 1, DTM_RIGHT = 2, DTM_LEFT = 3};


template <class T>
void SetBit(T& n, uint8_t nBitNum, bool bBit)
{
	if(bBit)
	{
		T i = 1;
		i <<= nBitNum;
		n |= i;
	}
	else
	{
		T i = 1;
		i <<= nBitNum;
		i = ~i;
		n &= i;
	}
}

template <class T>
bool GetBit(T n, uint8_t nBitNum)
{
	n >>= nBitNum;
	n &= 1;
	return n != 0;
}

template <class T>
void SetBitInfo(uint8_t* vArr, uint8_t& nPos, uint8_t& nBitPos, T nNum, uint8_t nBitSize)
{
	for(uint8_t i = 0; i < nBitSize; ++i)
	{
		bool b = GetBit(nNum, i);
		SetBit(vArr[nPos], nBitPos, b);

		++nBitPos;
		if(nBitPos >= 8)
		{
			nBitPos = 0;
			++nPos;
		}
	}
}

template <class T>
void GetBitInfo(uint8_t* vArr, uint8_t& nPos, uint8_t& nBitPos, T& nNum, uint8_t nBitSize)
{
	for(int i = 0; i < nBitSize; ++i)
	{
		bool b = GetBit(vArr[nPos], nBitPos);
		SetBit(nNum, i, b);

		++nBitPos;
		if(nBitPos >= 8)
		{
			nBitPos = 0;
			++nPos;
		}
	}
}

void Remove(list<char>& lsSend, char c)
{
	for(list<char>::iterator itr = lsSend.begin(), etr = lsSend.end(); itr != etr;)
	{
		if(*itr == c)
			lsSend.erase(itr++);
		else
			++itr;
	}
}


//--------------------------------------------------------------
void testApp::setup()
{	
	if(!srl.IsConnected())
	{
		std::cout << "Cannot connect!\n";
		OF_EXIT_APP(0);
	}

	ofBackground(255,255,255);	
	ofSetFrameRate(11);
}

//--------------------------------------------------------------
void testApp::update()
{
	if(apSt == AS_MOVEMENT_BUFFER && (ofGetElapsedTimeMillis() - nLastMovement) > 1000 * .25 && bMoveTrigger)
	{
		ConditionalMovement(DTM_FORWARD, 1, MOVEMENT_CNST * fMoveBuffer);
		fMoveBuffer = 0;
		apSt = AS_EMPTY;
	}
	
	if(apSt == AS_SONAR_ROTATE && (ofGetElapsedTimeMillis() - nLastMovement) > 1000 * .25 && bMoveTrigger)
	{
		if(sd.ss.vResults.size() == sd.ss.nNum)
		{
			apSt = AS_EMPTY;

			trjPath.vNodes.back().ss = sd.ss;
			sd.ss.vResults.clear();
		}
		else
		{
			apSt = AS_SONAR_FILL;
			sd.nSonarTimer = ofGetElapsedTimeMillis();
		}
	}

	if(apSt == AS_SONAR_FILL && (ofGetElapsedTimeMillis() - sd.nSonarTimer) > 100)
	{
		sd.nSonarTimer = ofGetElapsedTimeMillis();
		
		sd.sm.Add(nSonar);

		if(sd.sm.vResults.size() >= 7)
		{
			sd.ss.vResults.push_back(sd.sm.Extract());
			sd.sm.vResults.clear();

			gd.ApplySonar(SonarCone(GetPosition() + GetPoint(11, fAngle), fAngle, sd.ss.vResults.back()));

			apSt = AS_SONAR_ROTATE;
			
			nLastMovement = ofGetElapsedTimeMillis();
			bMoveTrigger = false;

			TurnTo(sd.fInitialDir + sd.ss.fInitialAngle + sd.ss.GetAngle()*sd.ss.vResults.size());
		}
	}
	
	if(lsSend.size())
	{
		char c = lsSend.front();
		if(c == 'w')
			nDir = DTM_FORWARD;
		else if (c == 's')
			nDir = DTM_BACK;
		else if(c == 'a')
			nDir = DTM_LEFT;
		else if(c == 'd')
			nDir = DTM_RIGHT;
		
		string s;
		s += c;

		if(!srl.WriteData(s.c_str(), s.size()))
		{
			std::cout << "Cannot write!\n";
			OF_EXIT_APP(0);
		}

		//std::cout << s << "\n";
	}

	while(true)
	{
		char nBuff[2];
		int nRead = srl.ReadData(nBuff, 1);

		if(!srl.IsConnected())
		{
			cout << "Disconnect on read!\n";
			OF_EXIT_APP(0);
		}

		if(nRead > 0)
		{
			if(nBuff[0] == 'd')
			{
				ofSleepMillis(10);
				char nData[3 + 1];
				nRead = srl.ReadData(nData, 3);
				if(nRead == 3)
				{
					int nRightDelta = int(unsigned char(nData[0]));
					int nLeftDelta = int(unsigned char(nData[1]));
					nSonar = ProcessSonar(int(unsigned char(nData[2])));

					fObstacle = 2.54*(nSonar / 2 + 1);

					if(nDir == DTM_RIGHT || nDir == DTM_LEFT)
					{
						if(fDistance != 0)
						{
							trjPath.AddNode(GetPosition());
							fDistance = 0;
						}
					}

					if(nRightDelta != 0)
					{
						if(nDir == DTM_RIGHT)
						{
							fAngle += float(nRightDelta) / RIGHT_TURN_CNST;
						}
						else if(nDir == DTM_LEFT)
						{
							fAngle -= float(nRightDelta) / LEFT_TURN_CNST;
						}
						else if(nDir == DTM_FORWARD)
						{
							fDistance += float(nRightDelta) / MOVEMENT_CNST;
						}
						else if(nDir == DTM_BACK)
						{
							fDistance -= float(nRightDelta) / MOVEMENT_CNST;
						}

						bMoveTrigger = true;
					}
					
					nRight += nRightDelta;
					nLeft += nLeftDelta;

					std::cout << nRight << "\t";
					std::cout << nLeft << "\t";
					std::cout << nSonar << "\t";
					std::cout << fAngle << "\t";
					std::cout << "\n";

					if(nRightDelta != 0)
						nLastMovement = ofGetElapsedTimeMillis();
				}
			}
			else if(nBuff[0] == 't')
			{
				while(true)
				{
					char nData[2];
					int nRead = srl.ReadData(nData, 1);

					if(!srl.IsConnected())
					{
						cout << "Disconnect on read!\n";
						OF_EXIT_APP(0);
					}

					if(nRead > 0)
					{
						std::cout << nData[0];
					}
					else
					{
						break;
					}
				}


			}
		}
		else
			break;
	}
}




void DrawSonar(float fRadius)
{
	while(fRadius > 0)
	{
		ofCircleSlice(0, 0, fRadius, -20 + 90, 20 + 90, true, false);
		fRadius -= 5;
	}
}

void DrawNodes(Trajectory& trjPath)
{
	ofPushMatrix();

	ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

	std::vector<Node>& v = trjPath.vNodes;

	for(unsigned i = 1; i < v.size(); ++i)
		ofLine(v[i-1].p.x, v[i-1].p.y, v[i].p.x, v[i].p.y);

	ofPopMatrix();
}

void DrawGrid(Grid& gd)
{
	ofPushMatrix();

	ofSetLineWidth(1);
	ofFill();

	ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

	for(unsigned i = 0, sz = gd.vData.size(); i < sz; ++i)
	{
		float f = exp(-pow(gd.vData[i], 2));
		int nColor = 255 - 100*f;

		ofSetColor(nColor, nColor, nColor);

		ofPoint p = gd.GetCoordinate(i);
		ofRect(p.x, p.y, gd.pSize.x, gd.pSize.y);
	}

	ofPopMatrix();
}


//--------------------------------------------------------------
void testApp::draw()
{
	DrawGrid(gd);

	ofPushMatrix();

	if(fDistance != 0)
		trjPath.AddNode(GetPosition());
	
	if(ofGetElapsedTimeMillis() - nTimer > 200)
	{
		nTimer = ofGetElapsedTimeMillis();

		if(trjPath.vNodes.size() > 1)
		{
			trjVariation = trjPath;

			for(int i = 0; i < 1000; ++i)
			{
				Trajectory trjWigg = trjVariation;
				
				int n = ofRandom(0, 3);

				if(n == 0)
					trjWigg.Wiggle(1, true);
				else if(n == 1)
					trjWigg.WiggleAngle(1);
				else
					trjWigg.WiggleDistance(1);
				
				if(GetVariance(trjPath, trjWigg) <= (trjPath.vNodes.size() - 1) * 2)
					trjVariation = trjWigg;
			}
		}
	}


	ofSetLineWidth(2);
	ofSetColor(100, 100, 100);

	DrawNodes(trjVariation);

	ofSetLineWidth(2);
	ofSetColor(0, 0, 0);

	DrawNodes(trjPath);


	ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
	ofTranslate(trjPath.vNodes.back().p.x, trjPath.vNodes.back().p.y);
	ofRotate(fAngle);
	
	if(fDistance != 0)
		trjPath.vNodes.pop_back();

	ofNoFill();
	ofRect(-11, -11, 22, 22);

	ofTranslate(0, -11);

	DrawSonar(fObstacle);
	
	ofPopMatrix();

}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{
	if(key == OF_KEY_UP)
	{
		if(bShift)
			nSpeed += 50;
		else
			nSpeed += 1;
		
		
		if(nSpeed > 250)
			nSpeed = 250;
		SpeedUpdate();
	}
	else if(key == OF_KEY_DOWN)
	{
		if(bShift)
			nSpeed -= 50;
		else
			nSpeed -= 1;

		if(nSpeed < 10)
			nSpeed = 10;
		SpeedUpdate();
	}
	else if(key == 'q')
		bShift = true;
	else if(key == ' ')
	{
		std::cout << nRight << "\t" << nLeft << "\n";
		nRight = nLeft = 0;
		fAngle = fDistance = 0;
		trjPath = trjVariation = Trajectory();
		gd.ClearGrid();
	}
	
	if(key == 'w' || key == 'a' || key == 's' || key == 'd')
	{
		Remove(lsSend, key);
		lsSend.push_front(key);
	}

	if(key == OF_KEY_RETURN)
	{
		sd.ss.vResults.clear();
		sd.ss.nNum = 9;
		sd.ss.fInitialAngle = 90;
		sd.ss.fScanAngle = 90;

		sd.fInitialDir = fAngle;

		apSt = AS_SONAR_ROTATE;
		
		nLastMovement = ofGetElapsedTimeMillis();
		bMoveTrigger = false;

		TurnTo(sd.fInitialDir + sd.ss.fInitialAngle);
	}

	if(key == 'i')
	{
		srl.WriteData("i", 1);
	}
}

void testApp::SpeedUpdate(bool bLeft, int nNum)
{
	std::cout << "Speed: " << nNum << "\n";
	
	string s;
	if(bLeft)
		s += '<';
	else
		s += '>';
	unsigned char cSpd = nNum;
	s += cSpd;

	if(!srl.WriteData(s.c_str(), s.size()))
	{
		std::cout << "Cannot write!\n";
		OF_EXIT_APP(0);
	}
}

void testApp::SpeedUpdate()
{
	SpeedUpdate(true, 200);
	ofSleepMillis(200);
	SpeedUpdate(false, nSpeed);
}


void testApp::ConditionalMovement(int nDir, int nEncoder, int nLimit)
{
	testApp::nDir = nDir;
	
	char nBuff[5];

	uint8_t nPos = 0;
	uint8_t nBitPos = 0;

	SetBitInfo(nBuff, nPos, nBitPos, 'c', 8);
	SetBitInfo(nBuff, nPos, nBitPos, nDir, 2);
	SetBitInfo(nBuff, nPos, nBitPos, nEncoder, 1);
	SetBitInfo(nBuff, nPos, nBitPos, nLimit, 11);
	SetBitInfo(nBuff, nPos, nBitPos, 3, 2);


	if(!srl.WriteData(nBuff, 3))
	{
		std::cout << "Cannot write!\n";
		OF_EXIT_APP(0);
	}
}

void testApp::TurnTo(int nAngle)
{
	float fDiff = AngleDiff(fAngle, float(nAngle));

	if(fDiff >= 0)
		ConditionalMovement(DTM_RIGHT, 1, int(fDiff * RIGHT_TURN_CNST));
	else
		ConditionalMovement(DTM_LEFT, 1, int(-fDiff * LEFT_TURN_CNST));
}




//--------------------------------------------------------------
void testApp::keyReleased(int key)
{
	if(key == 'q')
		bShift = false;
	
	if(key == 'w' || key == 'a' || key == 's' || key == 'd')
	{
		Remove(lsSend, key);
	}
}

//------------- -------------------------------------------------
void testApp::mouseMoved(int x, int y )
{
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{	
	if(button == 2)
	{
		ofPoint p(x, y);

		p.x -= ofGetWidth()/2;
		p.y -= ofGetHeight()/2;

		p -= GetPosition();

		TurnTo(GetAngle(p));

		nLastMovement = ofGetElapsedTimeMillis();
		bMoveTrigger = false;

		fMoveBuffer = ofDist(0, 0, p.x, p.y);
		apSt = AS_MOVEMENT_BUFFER;
	}
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{
}

ofPoint testApp::GetPosition()
{
	return trjPath.vNodes.back().p + GetPoint(fDistance, fAngle);
}
