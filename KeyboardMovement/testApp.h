#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"

#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <time.h>

#include "ArduinoSerial.h"

#include "Model.h"

using namespace std;

enum APP_STATE {AS_EMPTY, AS_MOVEMENT_BUFFER, AS_SONAR_FILL, AS_SONAR_ROTATE};

struct SonarData
{
	SonarMeasurement sm;
	SonarScan ss;
	float fInitialDir;

	int nSonarTimer;
};

class testApp : public ofBaseApp{
	
	public:

		testApp():srl("COM5"), nSpeed(200), bShift(false), fMoveBuffer(0), nLastMovement(0), apSt(AS_EMPTY), bMoveTrigger(false),
			nRight(0), nLeft(0), nDir(0), fAngle(0), fDistance(0), fObstacle(0), nTimer(0), nSonar(0),
			gd(100, 100, ofPoint(4,4), ofPoint(0,0))
		{}
		
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);


		list<char> lsSend;
		Serial srl;

		int nSpeed; // motors speed
		
		bool bShift;
		
		void SpeedUpdate();
		void SpeedUpdate(bool bLeft, int nNum);
		void ConditionalMovement(int nDir, int nEncoder, int nLimit);
		void TurnTo(int nAngle);

		int nRight;	// encoder cumulative data
		int nLeft;	

		int nDir;	// direction of current movement (for encoder tracking)

		float fAngle;	// current angle
		float fDistance;	// current distance traveled
		float fObstacle;	// distance to the obstacle
		int nSonar;			// sonar reading

		int nLastMovement;


		Trajectory trjPath;
		Trajectory trjVariation;

		int nTimer;

		APP_STATE apSt;
		bool bMoveTrigger;

		float fMoveBuffer;

		SonarData sd;

		Grid gd;

		ofPoint GetPosition();
};



#endif
	
