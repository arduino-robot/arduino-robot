# The name of our project is "KeyboardMovement". CMakeLists files in this project can 
# refer to the root source directory of the project as ${KeyboardMovement_SOURCE_DIR} and 
# to the root binary directory of the project as ${KeyboardMovement_BINARY_DIR}. 
cmake_minimum_required (VERSION 2.4) 

if(WIN32)

	project (KeyboardMovement)

	if(COMMAND cmake_policy)
	  cmake_policy(SET CMP0003 NEW)
	endif(COMMAND cmake_policy)

	ADD_DEFINITIONS(-DPOCO_STATIC)
	SET(CMAKE_EXE_LINKER_FLAGS /NODEFAULTLIB:/"atlthunk.lib, LIBC.lib, LIBCMT/")

	include_directories (${OF_ABS_PATH}/openFrameworks)
	include_directories (${OF_ABS_PATH}/openFrameworks/graphics)
	include_directories (${OF_ABS_PATH}/openFrameworks/app)
	include_directories (${OF_ABS_PATH}/openFrameworks/sound)
	include_directories (${OF_ABS_PATH}/openFrameworks/utils)
	include_directories (${OF_ABS_PATH}/openFrameworks/communication)
	include_directories (${OF_ABS_PATH}/openFrameworks/video)
	include_directories (${OF_ABS_PATH}/openFrameworks/events)
	include_directories (${OF_ABS_PATH}/glut/include)
	include_directories (${OF_ABS_PATH}/rtAudio/include)
	include_directories (${OF_ABS_PATH}/quicktime/include)
	include_directories (${OF_ABS_PATH}/freetype/include)
	include_directories (${OF_ABS_PATH}/freetype/include/freetype2)
	include_directories (${OF_ABS_PATH}/freeImage/include)
	include_directories (${OF_ABS_PATH}/fmodex/include)
	include_directories (${OF_ABS_PATH}/videoInput/include)
	include_directories (${OF_ABS_PATH}/glee/include)
	include_directories (${OF_ABS_PATH}/glu/include)
	include_directories (${OF_ABS_PATH}/poco/include)
	include_directories (${OF_ABS_PATH}/addons)

	link_directories (${OF_ABS_PATH}/glut/lib/vs2008)
	link_directories (${OF_ABS_PATH}/rtAudio/lib/vs2008)
	link_directories (${OF_ABS_PATH}/FreeImage/lib/vs2008)
	link_directories (${OF_ABS_PATH}/freetype/lib/vs2008)
	link_directories (${OF_ABS_PATH}/quicktime/lib/vs2008)
	link_directories (${OF_ABS_PATH}/fmodex/lib/vs2008)
	link_directories (${OF_ABS_PATH}/videoInput/lib/vs2008)
	link_directories (${OF_ABS_PATH}/glee/lib/vs2008)
	link_directories (${OF_ABS_PATH}/glu/lib/vs2008)
	link_directories (${OF_ABS_PATH}/poco/lib/vs2005)
	#link_directories (${OF_ABS_PATH}/openFrameworksCompiled/lib/vs2008)

	include_directories (${KeyboardMovement_SOURCE_DIR}/../include)

	add_executable(KeyboardMovement main.cpp testApp.cpp testApp.h ../include/ArduinoSerial.h ../include/ArduinoSerial.cpp Model.h Model.cpp)

	target_link_libraries(KeyboardMovement OFLIB)

endif(WIN32)

