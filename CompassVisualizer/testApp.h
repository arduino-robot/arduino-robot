#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"

#include <iostream>
#include <sstream>
#include <vector>
#include <list>
#include <string>
#include <time.h>

#include "ArduinoSerial.h"

using namespace std;




class testApp : public ofBaseApp{
	
	public:

		testApp():srl("COM6"){}
		
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);


		Serial srl;
		string sInputData;

		float fX;
		float fY;
		float fZ;

		void ProcessData();
};



#endif
	
