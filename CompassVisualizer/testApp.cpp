#include "testApp.h"

void Remove(list<char>& lsSend, char c)
{
	for(list<char>::iterator itr = lsSend.begin(), etr = lsSend.end(); itr != etr;)
	{
		if(*itr == c)
			lsSend.erase(itr++);
		else
			++itr;
	}
}

void testApp::ProcessData()
{
	istringstream istr(sInputData);

	istr >> fX >> fY >> fZ;
}



//--------------------------------------------------------------
void testApp::setup()
{	
	if(!srl.IsConnected())
	{
		std::cout << "Cannot connect!\n";
		OF_EXIT_APP(0);
	}

	ofSetFrameRate(11);

	fX = fY = fZ = 0;

	ofBackground(255,255,255);	
	ofSetLineWidth(2);
	ofSetColor(0, 0, 0);
}

//--------------------------------------------------------------
void testApp::update()
{
	while(true)
	{
		char nBuff[2];
		int nRead = srl.ReadData(nBuff, 1);

		if(!srl.IsConnected())
		{
			cout << "Disconnect on read!\n";
			OF_EXIT_APP(0);
		}

		if(nRead > 0)
		{
			sInputData += nBuff[0];

			if(nBuff[0] == '\n')
			{
				cout << sInputData;
				ProcessData();
				sInputData = "";
			}
		}
		else
			break;
	}
}


//--------------------------------------------------------------
void testApp::draw()
{
	ofLine(100, 100, 100, 100 - int(fZ/10));
	ofLine(200, 100, 200 + fX/10, 100 - int(fY/10));
}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{
}

//--------------------------------------------------------------
void testApp::keyReleased(int key)
{
}

//------------- -------------------------------------------------
void testApp::mouseMoved(int x, int y )
{
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{
}

